//controllers contains the functions and the bussiness logic of the express JS application
//all operation is in taskcontrollers.

const Task = require ("../Models/task") //we will put model in Task and require it and put the location of the model. Allow us to use the content of the task.js.

//[SECTION] - CONTROLLERS
//[SECTION] - Create
module.exports.createTask = (requestBody) => { //createTask will receive objects from the requestBody
	let newTask = new Task ({ //create/write new task
		name: requestBody.name
	})

	return newTask.save().then((task,error) => { //save the just inputed or the new task
		if (error) {
			return false
		} else {
			return task
		}
	})
}
//[SECTION] - Retreive
//1. create controller to retreive all task in the database
//2. when the taskCrontroller is called in the taskRoutes(task model) after it gets to getAllTask it will come here and find all the documents (find({}) - will find all documents). It will return the datas or result that it will find then go back in taskRoutes.
module.exports.getAllTasks = () => {
	//create a funtion that will retreive all task 
	return Task.find({}).then(result => { //return all the task, and then return also the result.
		return result
	})
}
//[SECTION] - Update
	//1. Change status of Task.pending -> 'completed'
	//we will reference the document using it's ID field.
	module.exports.taskCompleted = (taskId) => {
		//Query/search for the desired task ti update.
		//the "findById" mongoose method will look for a resource (task) which matches the ID from the URL of the request.
		//upon performing this method inside the collection a new state will be instantiated, we need to be able to handle the possible outcome of that state (.then((found,error)).
		return Task.findById(taskId).then((found,error) => {
			//descreibe how we are going to handdle the outcome of the state using selectin control structure
			//process the document found from the collection and change the status from pending to completed
			if (found) {
				//call out the parameter that describes the result of the quer when successfull.
				console.log(found); //document found from the database should be displayed in the terminal so you will know which element is being updated.
				//modify the status of the returned document to completed
				found.status = 'Completed'
				//Save the new changes inside the database.
				//upon saving the new changes for this document a second state will be instantiated.
				//we will chain a thenable expression upon performing a save() method into the returned document .then().
				return found.save().then((updatedTask,saveErr) => {
					//cath the state to identify a specific response.
					if (updatedTask) {
						return 'Task has been successfully modified'
					} else {
						//display/return the actual error (return saveErr)
						//or display a string that an error accur.
						return 'Task failed to update'
					}
				})
			} else {
				//
				return 'Error!, No document found';
			};

		}) ;
	};
	//create a route for PUT in taskRoute

	//2. Cahnge status of task (Completed -> pending)
	module.exports.taskPending = (userInput) => {
		//expose this new component (module.exports)
		//search the database for the userInput to see if there is a match.
		//findById mongoose method will run a query inside the database using the id fieldas its reference
		//since performing this methos will have 2 possible states we will a .then expression to handle the possible states (result,err)
		//assign and invoke this new controller task to its own separate route (taskRoute)
		return Task.findById(userInput).then((result,err) => {
			//handle and catch the state
			if (result) {
				//process the result of the query and extract the property to modify it value
				result.status = 'Pending'
				//save the new changes in the document, remove (`Match found`)
				return result.save().then((taskUpdated,error) =>{
					//create a control structure to identify the proper response if the updates is sucessful
					//taskUdated => object containing the parameters
					if (taskUpdated) {
						return `Task ${taskUpdated.name} is updated to pending`;
					} else {
						return `Error in saving task Updates`;
					}
				});
			} else {
				return `Something went wrong`;
			}

		})
	}
//[SECTION] - Delete
	//1. Remove an existing resource inside the task collection.
		//expose/export the data across other modules/other app so that it will become reusable.
		//we need to identify which resource we want to delete (we use ID (taskID) - like a reference).
	module.exports.deleteTask  = (taskId) => {
		//how will the data be proccessed in order to execute the task.
		//select which mongoose method will be used inorder to acquire the desired end goal.
		//Mongoose - provides an interface and methods to be able to manipulate the resources found inside the mongoDb atlas.
		//in order to identify the location in which the function will be executed append the model name(Task)
		//findByIdAndRemove - a mongoose method that target a documentfrom the collection, after it finds the document it will remove/delete that document.
		//then we will call the reference of the resource to be deleted (taskId)
		//upon executing this method within our collection this method will fall in either of this state: 
			//1. Pending - waiting to be excuted
			//2. Fulfilled - successfully executed
			//3. Rejected - the desired outcome did not happen
		//to be able to handle the possible states upon executing the task:
			//we are going to insert a "thenable" expression to deteremine HOW we will respond depending on the state it is currently on.(.then()).
			//identify the 2 possible state using a then expression(.then(fullfiled,rejected))

		return Task.findByIdAndRemove(taskId).then((fullfiled,rejected) => {
			if (fullfiled) {
				return 'The Task has been successfully Removed';
			} else {
				return 'Failed to remove Task';
			};
			//assing a new endpoint for this route.
				//go to taskRoute DELETING a task.

		});
	};

