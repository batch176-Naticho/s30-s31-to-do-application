//Contain all the endpoints for the application.
//We separate the routes such that "app.js" only contains information on the server.

let express = require("express")
//Create a router intance that function as a middleware and routing system.
//allow access to HTTP method middlewares that makes it easier to create routes for the application
let router = express.Router();

let taskController = require("../Controllers/taskController")// calling taskController in the controller folder. it connects with the taskcontroller in the other folder. (../ - two dots- magpinsan si taskRoute at si taskController)

//[ROUTES] - responsible in defining the endpoints/uri/url that is use to access the request.

//[GET] Routes for retreiving all the tasks
router.get("/", (req,res) => {
	//taskController.getAllTask() //the task controller contains the getAllTask() that contains the tasks in the collection.
	//taskController - contains the functions to be use when we access the endpoint
	//when we send a get request with the / endpoint, it runs the below code.

	//1. we call the taskController and inside the taskController it will look for the function getAllTask which is in taskController (there are many functions in taskController but it specifies to go in the getAllTask) ... (go to taskController [CONTROLLERS number 2]). whatever the result it will go to resultFromController(.then(resultFromController)- function) and it will display(res.send (resultFromController)) to the  client or in the postman
	taskController.getAllTasks().then(resultFromController =>
		res.send(resultFromController))
})

//[POST] route to CREATE task
router.post('/', (req,res) =>{
	//createTask(req.body) - will receive objects or datas from the request body.
	taskController.createTask(req.body).then(resultFromController =>res.send(resultFromController)) 
})

//[DELETE] Route for DELETING a task
	//call out the routing component to register a brand new endpoint.
	//when intergrating a path variable within the URI , you are also changing the behavior of the path from STATIC to DYNAMIC
	//2 Types of Endpoint
		//1. Staic route - endpoint does not change.
		//2. Dynamic - interchangable, not fixed,
	//How to chnage static to dynamic round
		//put a colon in the endpoint ('/:task') here and in the postman.

router.delete('/:task',(req,res) => {
	//identify the task to be executed within this endpoint.
	//callout the proper function for this route (deleteTask).
	//identify the source/provider of the function (taskController).
	//Determine wether the value inside the path variable is transmitted to the server.
	//params where the path variacble is located in the postman
	//task variable comes from params (params.task) section and params comes from request (req.params.task), we will print this out (console.log(req.params.task)).
	console.log(req.params.task)// ID should appear in the terminal.

	//place the value of the path variable inside its own container. comment out res.send
	//repackage req.params.task. disable res.send('Hello from delete'), uncomment taskController.deleteTask(
	let taskId = req.params.task
	//res.send('Hello from delete')//=> temporary response, this is to check if the setup is correct.

	//retreive the identity of the task by inserting the ObjectID of the resource.
	//make sure to pass down the identity of the task using the proper reference (ObjectId),(all datas came from request body) however this time we are going to include the informatiom as a path variable.
	//to insert path variables insert a colon (:) in the enpoint of the postman ({{4k}}tasks/:task - upon inserting the colon path variable will be active in the params section of postman)
		//Path Variable - allow us to insert data within the scope of the url.(store information within the url/path)
	//since colon is place before task, task becomes the variable that contains piece of information. Wherever the colon is append it will become the variable name.

	//When is a path variable useful?
		//when inserting only a single piece of information.
		//when passing down information to REST API method that does not include a body section like 'GET'. 
	//uncomment the console.log (temporary response)

	//upon executing this method a promise will be initialized so we need to handle the result of the promise(state) in our route module.
	taskController.deleteTask(taskId).then(resultOfDelete => res.send(resultOfDelete));

})

//[PUT] Route for Updating Status
	//lets create a DYNAMIC endpoint for this new route.
	router.put('/:task', (req,res) => {
		//check if you are able to acquire the values inside the path variables.
		console.log(req.params.task); //check if the value inside the parameters is properly transmitted to the server. at this point the value you put will appear int the terminal and postman will not stop sending since we do not have a terminating point.
		let taskId = req.params.task; //this is declared to simplify the means of calling out the value the path variable key.
		//identify tghe bussiness logic (go to taskController)
		//call the intended controller to execute the proccess, make sure to identify the provider/source of the function (taskController)
		//after invoking the correct controler method handle the outcome
		taskController.taskCompleted(taskId).then(outcome => res.send(outcome));
	});

//Route for updating task status (Completed -> pending)
	//this will be a counter procedure for the previuos task.
router.put('/:task/pending',(req,res) => {
	let id = req.params.task;
	//console.log(id); // test the initial setup, if the id appears in the server, cancel the send requestand comment this out.

	//declare the bussiness logic aspect of this new task in the app (taskController).
	//invoke the task you want to execute in this route
	taskController.taskPending(id).then(outcome => {
		res.send(outcome)
	})
})

//Use "module.exports" to export the router object to use in the "app.js"
//since we are using a module we need to export the routes for it to be use
module.exports = router;
