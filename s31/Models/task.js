//Create the schema/model
const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "Pending"  //if status is not filled a default value will appear.
	}
})
//model name = task
module.exports = mongoose.model("Task",taskSchema);