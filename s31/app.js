
//[SECTION] - Dependencies and Module
	const express = require("express")
	const mongoose = require("mongoose")
	//to access all routes
	const taskRoute = require("./routes/taskRoute"); //for app.js to access taskRoute (./ magkapatid si taskRoutes at si route)

//[SECTION] Server Setup
	const app = express();
	const port = 4000;
	app.use(express.json())
	//app.use(expressurlencoded({extended: true}))

//[SECTION] Database Setup
	mongoose.connect('mongodb+srv://rnaticho_07:admin123@cluster0.m9zfg.mongodb.net/toDo176?retryWrites=true&w=majority', {
		//only in node version 12 and 13 if node is updated no need for this (useNewUrlParser:true,useUnifiedTopology:true)
		useNewUrlParser:true,
		useUnifiedTopology:true
	});

	let db = mongoose.connection
	db.on('error',console.error.bind(console,"Connection Error"))
	db.once('open',()=>console.log("Connected to MongoDB"))

	//[SECTION] - Routing System
	//Add the task route
	//localhost:4000/tasks/ - / is the endpoint in the taskRoute
		app.use('/tasks', taskRoute);// everytime that there is a request in the task the server will know that we want to access the taskRoute. This is the main route, you have to append the request endpoint (POST endpoint=/ => url/path => http://localhost:4000/tasks/, for DELETE endpoint=/task => path => http://localhost:4000/tasks/task ) when adding a request.

		app.listen(port, () => console.log(`Server running at port: ${port}`))