//[SECTION] Dependencies and Modules
	const express = require("express")
	const mongoose = require("mongoose")

//[SECTION] Server Setup
	//establish a connection
	const app = express();
	//define a path/address. Assign port to 4000 to avoid conflict in the future with frontend applications that run on port 3000.
	const port = 4000;


//[SECTION] Database Connection
	//connect to MongoDB Atlas
	//change the password bracket to your own database
	//mongoDB will automaticcaly create an unregistered database
		//Get the credentials of the atlas user
		//ODM waiter of database to API
			//mongoose is an ODM library that connects database to server.
	mongoose.connect('mongodb+srv://rnaticho_07:admin123@cluster0.m9zfg.mongodb.net/toDo176?retryWrites=true&w=majority', {
		//option to add to avoid depreciation warnings because of mongoose/mongoDB udate.
		useNewUrlParser:true,
		useUnifiedTopology:true
	});
	//Create a notification to notify if the connection to the database is a success or a failure.
	let db = mongoose.connection;
	//add an on() method from the mongoose connection to show if the connection has succeeded or failed in both the terminal and in the browser for our clients
	db.on('error',console.error.bind(console,"Connection Error"));
	//once the connection is open and successfull, a message will appear in the terminal.
	db.once('open',()=>console.log("Connected to MongoDB"));

	//middleware - in express js context, methods/function that acts and adds features to the application.
	app.use(express.json());

//--------------------------------------ACTIVITY---------------------------------------------------//
 const userSchema = new mongoose.Schema ({
 	username:String,
 	password:String
 })
 const user = mongoose.model("user", userSchema);
 app.post('/users',(req,res)=> {
 	let newUser = new user({
 		username: req.body.username,
 		password: req.body.password
 	})
	newUser.save()	
	.then(result => res.send({message: "Document creation successful"}))
	.catch(error => res.send({message: "Error in document creation"}))
 })

 app.get('/users',(req,res) => {
 	user.find({})
 .then(result => res.send (result))
 .catch(error => res.send (error))
 })


//----------------------------------------------------------------------------------------------//

//[SECTION] Entry Point Response
	//build the connection to the designated port.
	app.listen(port, () => console.log(`Server running at port: ${port}`));



